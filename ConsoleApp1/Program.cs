﻿using ConsoleApp1.Model;
using ConsoleApp1.View;
using ConsoleApp1.ViewModel;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            ModelClass modelClass = new ModelClass();
            ViewClass viewClass = new ViewClass();
            ViewModelClass viewModelClass = new ViewModelClass();
            viewClass._viewModelClass = viewModelClass;
            viewModelClass._modelClass = modelClass;
            viewModelClass._viewClass = viewClass;

            viewClass.GameCycle();
        }
    }
}
