﻿using ConsoleApp1.ViewModel;

namespace ConsoleApp1.View
{
    class ViewClass
    {
        public ViewModelClass _viewModelClass;

        public void GameCycle()
        {
            while (_viewModelClass.GameState)
            {
                _viewModelClass.Message = System.Console.ReadLine();
            }
        }

        public void Message(string fancyMessage)
        {
            System.Console.WriteLine($"Message: {fancyMessage}");
        }
    }
}
