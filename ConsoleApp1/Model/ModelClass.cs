﻿namespace ConsoleApp1.Model
{
    class ModelClass
    {
        public ModelClass()
        {
            GameState = true;
            FancyData = "I'm the Fancy Data";
        }

        public bool GameState { get; set; }

        public string FancyData { get; set; }
    }
}
