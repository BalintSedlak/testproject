﻿using ConsoleApp1.Model;
using ConsoleApp1.View;

namespace ConsoleApp1.ViewModel
{
    class ViewModelClass
    {
        public ModelClass _modelClass;
        public ViewClass _viewClass;

        public bool GameState
        {
            get { return _modelClass.GameState; }
            set { _modelClass.GameState = value; }
        }

        public string Message
        {
            set
            {
                _modelClass.FancyData = value;
                _viewClass.Message(_modelClass.FancyData);
            }
        }

    }
}
